#!/bin/sh

ln -s -f ../../hooks/pre-push.sh .git/hooks/pre-push

flake8 --install-hook=git
chmod +x .git/hooks/pre-commit
