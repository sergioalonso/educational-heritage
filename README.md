Educational Heritage

[![build status](https://gitlab.com/sergioalonso/educational-heritage/badges/master/build.svg)](https://gitlab.com/sergioalonso/educational-heritage/commits/master)
[![coverage report](https://gitlab.com/sergioalonso/educational-heritage/badges/master/coverage.svg)](https://gitlab.com/sergioalonso/educational-heritage/commits/master)
[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/423/badge)](https://bestpractices.coreinfrastructure.org/projects/423)
