"""Register repository model into admin page."""
from django.contrib import admin

from .models import Item

admin.site.register(Item)
