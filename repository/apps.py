"""Repostory app."""

from django.apps import AppConfig


class RepositoryConfig(AppConfig):
    """Repository config."""

    name = 'repository'
