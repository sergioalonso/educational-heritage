"""Repository model."""

from django.db import models


class Item(models.Model):
    """Item model."""

    title = models.TextField(default='')
    description = models.TextField(default='')
    image = models.TextField(default='')
    url = models.TextField(default='')
