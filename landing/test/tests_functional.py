"""Landing Functional Tests."""

import unittest

from django.test import tag
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


@tag('functional')
class OpenEducationalResourcesTest(unittest.TestCase):
    """Open Educational Resources Functional Test Suite."""

    def setUp(self):
        """Test Case set up."""
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()

        self.browser = webdriver.Chrome()

    def tearDown(self):
        """Test Case tear down."""
        self.browser.close()
        self.display.stop()

    def test_subscribe_into_landing_page(self):
        """Test Case: home page."""
        # User goes to check out homepage
        self.browser.get('https://127.0.0.1:8000')

        # She notices the page title and header mention Educational Resources
        self.assertIn('Recursos Educativos', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Personas mejorando la educación', header_text)

        # She is invited to enter an email address
        inputbox = self.browser.find_element_by_id('id_email')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'nombre@dominio.com'
        )

        # She types her email address into a text box
        inputbox.send_keys('test.0@demo.com')

        # When she hits enter, the page is redirected to a thanks page
        inputbox.send_keys(Keys.ENTER)

        self.assertIn('https://127.0.0.1:8000/landing/', self.browser.current_url)

        rows = self.browser.find_elements_by_tag_name('p')
        self.assertTrue(
            any(row.text == 'Gracias por subscribirte test.0@demo.com' for row in rows)
        )
