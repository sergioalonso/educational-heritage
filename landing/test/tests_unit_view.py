"""Landing app unit testing view."""

from django.test import TestCase
from django.urls import reverse

from landing.forms import SubscriberForm
from landing.models import Subscriber


class HomePageTest(TestCase):
    """Home Page Test Suite."""

    def test_home_page_renders_home_template(self):
        """Test Case: Home page."""
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'landing/index.html')

    def test_home_page_uses_subscriber_form(self):
        """Test Case: Home page form."""
        response = self.client.get('/')
        self.assertIsInstance(response.context['form'], SubscriberForm)


class SubscribePageTest(TestCase):
    """Subscribe Page Test Suite."""

    def test_saving_a_post_request(self):
        """Test Case: correct post. Valid email is subscribed."""
        self.client.post(
            '/landing/subscribe/',
            data={'email': 'test.1@demo.com'}
        )
        self.assertEqual(Subscriber.objects.count(), 1)
        new_item = Subscriber.objects.first()
        self.assertEqual(new_item.email, 'test.1@demo.com')

    def test_handling_bad_post_request(self):
        """Test Case: bad post. Invalid email is not subscribed."""
        self.client.post(
            '/landing/subscribe/',
            data={'email': ''}
        )
        self.assertEqual(Subscriber.objects.count(), 0)

    def test_no_post_request(self):
        """Test Case: no post. Redirect to home page."""
        response = self.client.get('/landing/subscribe/')
        self.assertRedirects(response, '/landing/')


class ThanksViewTests(TestCase):
    """Thanks page test suite."""

    def test_no_subscriber(self):
        """Unknow subscriber."""
        url = reverse('landing:thanks', kwargs={'subscriber_uuid': '11111111-1111-1111-1111-111111111111'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_subscriber(self):
        """Existing subscriber."""
        subscriber = Subscriber.objects.create(email="demo.user@domain.com")

        url = reverse('landing:thanks', kwargs={'subscriber_uuid': subscriber.uuid})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
