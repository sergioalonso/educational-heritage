"""Landing models."""

from django.test import TestCase
from landing.models import Subscriber


class SubscriberModelTest(TestCase):
    """Subscriber model."""

    def test_saving_and_retreieving_subscribers(self):
        """Test Case: saving and retreiving subscribers."""
        s0 = Subscriber()
        s0.email = "subscriber0@domain.com"
        s0.save()

        s1 = Subscriber()
        s1.email = "subscriber1@domain.com"
        s1.save()

        subscribed = Subscriber.objects.all()
        self.assertEqual(subscribed.count(), 2)

        self.assertEqual(subscribed[0].email, 'subscriber0@domain.com')
        self.assertEqual(subscribed[1].email, 'subscriber1@domain.com')

        self.assertEqual(subscribed[0].__str__(), subscribed[0].email)
        self.assertEqual(subscribed[1].__str__(), subscribed[1].email)
