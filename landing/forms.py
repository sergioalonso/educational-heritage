"""Landing forms."""

from django import forms


class SubscriberForm(forms.Form):
    """Subscriber form."""

    email = forms.EmailField(
        label='',
        max_length=254,
        widget=forms.EmailInput(attrs={'class': "form-control input-lg",
                                       'placeholder': "nombre@dominio.com"}),
    )
