# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-02 13:25
"""Migration."""

from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    """Create model."""

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=254)),
                ('date', models.DateTimeField(verbose_name='date subscribed')),
            ],
        ),
    ]
