"""Landing views."""

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse

from .forms import SubscriberForm
from .models import Subscriber


def index(request):
    """Home page."""
    form = SubscriberForm()
    return render(request, 'landing/index.html', {'form': form})


def thanks(request, subscriber_uuid):
    """Subscribed page."""
    subscriber = get_object_or_404(Subscriber, pk=subscriber_uuid)
    context = {'subscriber': subscriber}
    return render(request, 'landing/thanks.html', context)


def subscribe(request):
    """Subscribe page."""
    if request.method == 'POST':
        form = SubscriberForm(request.POST)
        if form.is_valid():
            subscriber, created = Subscriber.objects.get_or_create(
                email=form.cleaned_data['email'])
            return HttpResponseRedirect(reverse('landing:thanks', kwargs={'subscriber_uuid': subscriber.uuid}))

        return render(request, 'landing/index.html', {'form': form})

    return HttpResponseRedirect(reverse('landing:index'))
