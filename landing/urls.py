"""Landing urls."""

from django.conf.urls import url

from . import views

app_name = 'landing'

urlpatterns = [
    # /landing/
    url(r'^$', views.index, name='index'),
    # /landing/subscribe/
    url(r'^subscribe/$', views.subscribe, name='subscribe'),
    # /landing/3f9895b9-1ad8-4b63-bac3-45225d585502/
    url(r'^(?P<subscriber_uuid>[0-9a-z-]+)/$', views.thanks, name='thanks'),
]
