"""Gunicorn settings."""

bind = "0.0.0.0:8000"

workers = 5

# Asynchronous workers
worker_class = "eventlet"
