Educational Heritage Contribution Guide

# Source Code Repository

    sudo apt-get install -y git

    git clone git@gitlab.com:sergioalonso/educational-heritage.git

# Python Language

    sudo apt-get install -y python python-pip python-dev

    sudo apt-get install -y python3.4 python3-pip python3.4-dev


    sudo pip install virtualenvwrapper

    sudo pip3 install virtualenvwrapper


    mkvirtualenv -a ~/src/educational-heritage educational-heritage

    workon educational-heritage


    pip install -r requirements.txt

# SQLite Database

    python manage.py makemigrations --settings=educational_heritage.settings.local

    python manage.py migrate --settings=educational_heritage.settings.local

## Command Line Shell For SQLite3

    sudo apt-get install -y sqlite3

    sqlite3 educational_heritage/db.sqlite3

    sqlite>
    .tables
    select * from landing_subscriber;
    .exit

## Create Admin User

     echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@example.com', '1dm3n')" | python manage.py shell --settings=educational_heritage.settings.local

## django-allauth SQLite3 Data

    sqlite3 educational_heritage/db.sqlite3 < sql/django-allauth.sql

# Django Framework

## Environment Variables

    echo "export SECRET_KEY=\"$(python -c 'import random; import string; print("".join([random.SystemRandom().choice("{}{}{}".format(string.ascii_letters, string.digits, string.punctuation)) for i in range(50)]))')\"" >> ~/.virtualenvs/educational-heritage/bin/postactivate

## Run Development Server [deprecated][Run Development Server Using HTTPS]

    python manage.py runserver --settings=educational_heritage.settings.local 0.0.0.0:8000

## Run Development Server Using HTTPS

    python manage.py runserver_plus --cert /tmp/cert --settings=educational_heritage.settings.local 0.0.0.0:8000

# Development Process: Test Driven Development

    sudo ./bin/selenium-install.sh

## Load Testing Data

    python manage.py loaddata Item --settings=educational_heritage.settings.local

## Unit Tests

    coverage run manage.py test -v 2 --settings=educational_heritage.settings.local --exclude-tag=functional --exclude-tag=external

## Functional Tests

    [Run server][Run Development Server Using HTTPS]

    python3 manage.py test --settings=educational_heritage.settings.local --tag=functional --exclude-tag=external

## External Servers

    coverage run manage.py test -v 2 --settings=educational_heritage.settings.local --tag=external

## Code Coverage

    coverage run manage.py test -v 2 --settings=educational_heritage.settings.local --exclude-tag=external

    coverage report -m --skip-covered

    coverage html

    browse htmlcov/index.html

# Development Discipline: Continuous Delivery

https://continuousdelivery.com/

# Branch Model: Trunk Based Development

http://paulhammant.com/2013/04/05/what-is-trunk-based-development/

# Coding Style
## Automate Check Style With Git Hooks

pre-push hook

    cp hooks/pre-push.sh .git/hooks/pre-push

pre-commit hook

    flake8 --install-hook=git

## EditorConfig

    sudo apt-get install -y editorconfig

    .editorconfig

## Python PEP8

    flake8 .

## Docstring Conventions PEP 257

The flake8-docstrings extension will check docstrings according to PEP257 when running flake8

    flake8 .

## Django Coding Style

https://docs.djangoproject.com/en/dev/internals/contributing/writing-code/coding-style/

## Google Python Style Guide

https://google.github.io/styleguide/pyguide.html

# Documentation

    sudo apt-get install -y pandoc

    git clone git@gitlab.com:sergioalonso/educational-heritage.wiki.git

    mkvirtualenv --python=python2.7 -a ~/src/educational-heritage.wiki educational-heritage.wiki

    pip install -r requirements.txt

    python -m plantuml file.uml

# Docker

    docker version
    docker help

## List containers

    docker ps
    docker ps -l

## Stop all docker containers

    docker stop $(docker ps -a -q)

## Remove all docker continers

    docker rm $(docker ps -a -q)

## Remove all docker images

    docker rmi --force $(docker images -q)

## Run an interactive container

    docker run --name educational-heritage-staging \
    -i -t -e SECRET_KEY=$SECRET_KEY \
    registry.gitlab.com/sergioalonso/educational-heritage:staging \
    /bin/bash

## Run a detached container

    docker rm educational-heritage-staging

    docker run --name educational-heritage-staging \
    -d -p 8000:8000 -e SECRET_KEY=$SECRET_KEY \
    registry.gitlab.com/sergioalonso/educational-heritage:staging \
    python manage.py runserver --settings=educational_heritage.settings.local 0.0.0.0:8000

## Get IP

    docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' educational-heritage-staging

## Browse the application

    w3m http://172.17.0.2:8000

# Docker Compose

    docker-compose build
    docker-compose up -d
    docker run -e SECRET_KEY=$SECRET_KEY -it -p 8000:8000 educationalheritage_web /bin/bash
    docker-machine ip devel1

# Gitlab Docker Registry

    docker login registry.gitlab.com

# Bash session inside the running container

    docker exec -it educational-heritage /bin/bash
