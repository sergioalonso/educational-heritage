"""Production settings."""

import os

from .base import *  # noqa

DEBUG = False

ALLOWED_HOSTS = ['legadoeducativo.com', 'staging.legadoeducativo.com', 'www.legadoeducativo.com']

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = os.environ["GMAIL_USERNAME"]
EMAIL_HOST_PASSWORD = os.environ["GMAIL_PASSWORD"]
EMAIL_USE_TLS = True
