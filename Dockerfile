FROM python:3.4-slim

# Update OS
RUN apt-get update
RUN apt-get -y upgrade
RUN apt-get -y dist-upgrade


RUN LC_ALL=C DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y \
            apt-utils \
            gcc \
            gettext \
            git \
            libpq-dev \
            postgresql-client \
            sqlite3 \
            make \
            supervisor \
            --no-install-recommends && \
            rm -rf /var/lib/apt/lists/*

ENV DJANGO_VERSION 1.10

RUN pip install psycopg2 django=="$DJANGO_VERSION"


RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/

ARG SECRET_KEY
ARG ADMIN_PASSWORD
ARG DROPBOX_SECRET
ARG DROPBOX_CLIENT_ID
ARG DROPBOX_KEY

RUN make configure
RUN make db
RUN make static


EXPOSE 8000

CMD ["/usr/bin/supervisord"]
