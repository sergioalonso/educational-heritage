.PHONY: help configure db run test clean

default: help

DATABASE_NAME="educational_heritage/db.sqlite3"

help:
	@echo "help - Show this help"
	@echo "configure - Create configuration files"
	@echo "db - Init database"
	@echo "static - Collect static files"
	@echo "test - Run tests"
	@echo "run - Start server"
	@echo "clean - Remove temporal files"

configure:
	python configure.py

db:
	rm -f $(DATABASE_NAME)
	python manage.py makemigrations --settings=educational_heritage.settings.local
	python manage.py migrate --settings=educational_heritage.settings.local
	sqlite3 $(DATABASE_NAME) < django_allauth.sql

static:
	python manage.py collectstatic --no-input --settings=educational_heritage.settings.local

test:
	coverage run ./manage.py test -k -v 2 --settings=educational_heritage.settings.local --exclude-tag=external --exclude-tag=functional

run:
	python manage.py runserver_plus --cert cert.crt --settings=educational_heritage.settings.local 0.0.0.0:8000

deploy-staging:
    # ./certbot-auto certonly --webroot -w . -d staging.legadoeducativo.com

	docker stop educational-heritage-staging
	docker rm educational-heritage-staging
	docker pull "registry.gitlab.com/sergioalonso/educational-heritage:staging"
	docker run --restart=always -d -e "VIRTUAL_HOST=staging.legadoeducativo.com" -e "VIRTUAL_PROTO=https" -p 8000:8000 --env-file educational-heritage.env \
		--name educational-heritage-staging registry.gitlab.com/sergioalonso/educational-heritage:staging python manage.py runserver_plus \
		--cert cert --settings=educational_heritage.settings.production 0.0.0.0:8000
	docker exec educational-heritage-staging make configure
	docker exec educational-heritage-staging make db

clean:
	find . -name "*~" -print -delete
	find . -name "*.pyc" -print -delete
	rm -f ./django_allauth.sql
	rm -fr ./htmlcov/
