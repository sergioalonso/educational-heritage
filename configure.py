#!/bin/env
"""Help new users configure the database for use with social networks."""

import os
from datetime import datetime

import django
from django.conf import settings
from django.template import Context, Template

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

settings.configure(DEBUG=True,
                   TEMPLATE_DEBUG=True,
                   TEMPLATES=[{'BACKEND': 'django.template.backends.django.DjangoTemplates'}],
                   TEMPLATE_DIRS=(os.path.join(BASE_DIR, 'templates/')))
django.setup()


sql_template = Template("""
UPDATE django_site SET DOMAIN = '127.0.0.1:8000', name = 'educationalheritage' WHERE id=1;

{% if admin %}
--
-- superuser
--
DELETE from auth_user;
INSERT INTO auth_user(id, password, last_login, is_superuser, first_name, last_name, email, is_staff, is_active, date_joined, username)
VALUES (1, '{{admin.password}}', '{{now}}', 1, '{{admin.first_name}}',
        '{{admin.last_name}}', '{{admin.email}}', 1, 1, '{{now}}', '{{admin.username}}');
{% endif %}

{% if dropbox %}
--
-- socialapp_sites
--
DELETE FROM socialaccount_socialapp_sites;
{% endif %}

{% if dropbox %}
--
-- Dropbox
--
DELETE FROM socialaccount_socialapp WHERE provider='dropbox_oauth2';
INSERT INTO socialaccount_socialapp (provider, name, secret, client_id, `key`)
VALUES ("dropbox_oauth2", "Dropbox", "{{dropbox.secret}}", "{{dropbox.client_id}}", "{{dropbox.key}}");

INSERT INTO socialaccount_socialapp_sites (socialapp_id, site_id) VALUES (
  (SELECT id FROM socialaccount_socialapp WHERE name='Dropbox'),1);
{% endif %}
""")


def admin():
    """Generate admin user values."""
    from django.contrib.auth.hashers import make_password
    username = "admin"
    first_name = "Sergio"
    last_name = "Alonso"
    email = "sergio@legadoeducativo.com"

    if 'ADMIN_PASSWORD' not in os.environ:
        raise Exception("No ADMIN_PASSWORD environment variable found.")

    password = os.getenv("ADMIN_PASSWORD")
    password = make_password(password)

    return dict(first_name=first_name, last_name=last_name, email=email, password=password, username=username)


def dropbox():
    """Generate dropbox values."""
    if 'DROPBOX_SECRET' not in os.environ:
        raise Exception("No DROPBOX_SECRET environment variable found.")

    if 'DROPBOX_CLIENT_ID' not in os.environ:
        raise Exception("No DROPBOX_CLIENT_ID environment variable found.")

    if 'DROPBOX_KEY' not in os.environ:
        raise Exception("No DROPBOX_KEY environment variable found.")

    secret = os.getenv('DROPBOX_SECRET')
    client_id = os.getenv('DROPBOX_CLIENT_ID')
    key = os.getenv('DROPBOX_KEY')

    return dict(secret=secret, client_id=client_id, key=key)


if __name__ == "__main__":

    context = Context({
        'now': str(datetime.now())
    })

    context['admin'] = admin()
    context['dropbox'] = dropbox()

    with open('django_allauth.sql', 'w') as out:
        out.write(sql_template.render(context))

    print("\nDone! Check it:\n")
    print("cat django_allauth.sql\n")
