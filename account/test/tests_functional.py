"""Account funtional tests suite."""

import json
import unittest

from django.test import tag
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


@tag('functional', 'external')
class AccountTest(unittest.TestCase):
    """Account Test Suite."""

    fixtures = ['allauth_fixture']

    def setUp(self):
        """Test Case set up."""
        self.display = Display(visible=0, size=(1024, 768))
        self.display.start()

        self.browser = webdriver.Chrome()
        self.browser.wait = WebDriverWait(self.browser, 10)

    def tearDown(self):
        """Test Case tear down."""
        self.browser.close()
        self.display.stop()

    def get_element_by_id(self, element_id):
        """Get element by id."""
        return self.browser.wait.until(EC.presence_of_element_located((By.ID, element_id)))

    def get_element_by_name(self, element_name):
        """Get element by xpath."""
        return self.browser.wait.until(EC.presence_of_element_located((By.XPATH, element_name)))

    def user_login(self):
        """Login user into dropbox."""
        with open("account/fixtures/dropbox_user.json") as f:
            credentials = json.loads(f.read())
        self.get_element_by_name("//input[@name='login_email']").send_keys(credentials["Email"])
        self.get_element_by_name("//input[@name='login_password']").send_keys(credentials["Passwd"])
        self.get_element_by_name("//button[@type='submit']").click()
        return

    def test_login_with_dropbox(self):
        """Test Case: login with dropbox."""
        # User goes to checkout the Open Educational Resources account
        self.browser.get('https://127.0.0.1:8000/')

        # Check that the login button is present
        dropbox_login = self.get_element_by_id("dropbox_login")

        # Check that login button points to correct url
        self.assertEqual(
            dropbox_login.get_attribute("href"),
            "https://127.0.0.1:8000/accounts/dropbox_oauth2/login/")

        # checks that after clicking on the login button
        dropbox_login.click()

        # could enter dropbox user credentials
        self.user_login()

        # the user gets logged in and it sees the logout button instead
        dropbox_logout = self.get_element_by_id("logout")

        # click on the logout button
        dropbox_logout.click()
        # should make the user see the login button again.
        dropbox_login = self.get_element_by_id("dropbox_login")
