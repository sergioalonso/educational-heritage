"""include any application configuration for the app."""

from django.apps import AppConfig


class AccountConfig(AppConfig):
    """Account app configuration."""

    name = 'account'
    label = 'org.educationalheritage.account'
