"""Account models."""

import hashlib

from allauth.account.models import EmailAddress
from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    """UserProfile model."""

    user = models.OneToOneField(User, related_name='profile')

    def __unicode__(self):
        """Render a string representation."""
        return "{}'s profile".format(self.user.username)

    class Meta:
        """Model metadata."""

        db_table = 'user_profile'

    def account_verified(self):
        """Detect the user’s email-verification status."""
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False

    def profile_image_url(self):
        """Display the user's gravatar icon."""
        return "http://www.gravatar.com/avatar/{}?s=40".format(hashlib.md5(self.user.email.encode('utf-8')).hexdigest())

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])
